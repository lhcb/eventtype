# eventtype

Online tool to help ease the confusion that comes with LHCb event type.

The goal is to provide both 
 - a tool that deciphers the meaning of a given event type and 
 - a wizard that helps find the correct event type of a decay

## deployment

```bash
docker build --file nginx/Dockerfile . --tag gitlab-registry.cern.ch/lhcb/eventtype/nginx:latest
docker build --file Dockerfile . --tag gitlab-registry.cern.ch/lhcb/eventtype/django:latest
docker push gitlab-registry.cern.ch/lhcb/eventtype/django:latest
docker push gitlab-registry.cern.ch/lhcb/eventtype/nginx:latest
# login to openshift on lxplus
oc project lhcb-eventtype
oc import-image django --from gitlab-registry.cern.ch/lhcb/eventtype/django:latest
oc import-image nginx --from gitlab-registry.cern.ch/lhcb/eventtype/nginx:latest
```

## Running locally

```bash
docker network create eventtype
docker run --network eventtype -e SECRET_KEY=i-am-not-a-secret-change-me -p 8123:8123 --name django gitlab-registry.cern.ch/lhcb/eventtype/django:latest
docker run --rm -it --network eventtype -p 8124:8124 --name nginx gitlab-registry.cern.ch/lhcb/eventtype/nginx:latest
```
