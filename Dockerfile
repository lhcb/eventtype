FROM python

RUN pip install django gunicorn
COPY ./eventtype/ /backend/
ENV PYTHONPATH=/backend
EXPOSE 8123
CMD ["gunicorn", "eventtype.wsgi:application", "--bind", "0.0.0.0:8123", "--workers", "3"]
